#pragma once
#include<array>
#include<optional>
#include "Piece.h"

class Board
{
public:
	//typedef std::pair< uint8_t, uint8_t> Position;
	using Position = std::pair< uint16_t, uint16_t>;
public:
	const std::optional<Piece>& operator[](const Position& position) const;
	std::optional<Piece>& operator[](const Position& position);
	friend std::ostream& operator<<(std::ostream& out, const Board& board);

private:
	static const uint8_t columnNumber = 4;
	static const uint8_t lineNumber = 4;
private:
	//std::array<std::array<Piece, 4>, 4> m_board;
	//std::array<std::array<Piece*, 4>, 4> m_board;
	//std::array<std::array<std::pair<bool,Piece>, 4>, 4> m_board;
	std::array<std::array<std::optional<Piece>, columnNumber>, lineNumber> m_board;//trebue visual studio 2017, din properties
};

