#include "Player.h"

Player::Player(const std::string& name): m_name(name)
{
}

Piece Player::PickPiece(std::istream& in, UnusedPieces& unusedPiece)
{
	std::string pieceName;
	in >> pieceName;
	return unusedPiece.PickPiece(pieceName);
}

void Player::PlacePiece(std::istream& in, Board& board, Piece&& piece)
{
	Board::Position position;
	auto& [line, column] = position;
	in >> line;
	if (in.good())
	{
		if (in >> column)
		{
			board[position] = std::move(piece);
			return;
		}
	}
	throw std::logic_error("Could not read the line or column !");
}

std::ostream& operator<<(std::ostream& out, const Player& player)
{
	out << player.m_name;
	return out;
}
