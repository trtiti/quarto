#include "Board.h"

const std::optional<Piece>& Board::operator[](const Position& position) const
{
	auto& [line, column] = position;//structured binding
	return m_board[line][column];
}

std::optional<Piece>& Board::operator[](const Position& position)
{
	auto& [line, column] = position;//structured binding
	if (line < 0 || line >= lineNumber || column < 0 || column >= columnNumber)
		throw std::out_of_range("Line or column out of bounds !");
	if (m_board[line][column].has_value())
		throw std::invalid_argument("Position already occupied !");
	return m_board[line][column];
}

std::ostream& operator<<(std::ostream& out, const Board& board)
{
	for (const auto& line : board.m_board)
	{
		for (const auto& pieceOptional : line)
		{
			//if (pieceOptional)
				//out << *pieceOptional;
			if (pieceOptional.has_value())
				out << pieceOptional.value();
			else
				out << "________";
			out << " ";
		}
		out << "\n";
	}
		
	return out;

}
