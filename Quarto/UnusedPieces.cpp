#include "UnusedPieces.h"
#include<sstream>
#include<array>
UnusedPieces::UnusedPieces()
{
	//Piece piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Tall, Piece::Shape::Round);
	//EmplacePiece(piece);
	GeneratePieces();
}

Piece UnusedPieces::PickPiece(const std::string& name)
{
	/*auto it = m_pieces.find(name);
	if (it == m_pieces.end())
	{
		throw "Could not find piece!";
	}
	Piece piece = it->second;
	m_pieces.erase(it);
	return piece;*/
	auto node = m_pieces.extract(name);
	if (node.empty())
	{
		throw "Could not find piece!";
	}
	return std::move(node.mapped());
}

void UnusedPieces::EmplacePiece(Piece piece)
{
	std::stringstream streamString;
	streamString << piece;
	m_pieces.insert(std::make_pair(streamString.str(), std::move(piece)));
}

std::ostream& operator<<(std::ostream& out, const UnusedPieces& unusedPieces)
{
	//for (auto pair : unusedPieces.m_pieces)
	//{
	//	/*out << pair.first << "\n";*/
	//}
	for (auto& [name, piece] : unusedPieces.m_pieces)
	{
		out << name << "\n";
	}
	return out;
}
void UnusedPieces::GeneratePieces()
{
	const uint8_t kPermutationPoolSize = 8;
	std::array<uint8_t, kPermutationPoolSize> permutationPool = { 1, 1, 1, 1, 2, 2, 2, 2 };
	const uint8_t kMaxPermutations = 5;
	const uint8_t kPermutationSize = 4;

	for (uint8_t currentPermutation = 0; currentPermutation < kMaxPermutations; ++currentPermutation)
	{
		auto itPoolBegin = permutationPool.begin() + currentPermutation;
		auto itPoolEnd = permutationPool.begin() + currentPermutation + kPermutationSize;

		do
		{
			EmplacePiece(Piece(
				static_cast<Piece::Body>(*itPoolBegin),
				static_cast<Piece::Color>(*(itPoolBegin + 1)),
				static_cast<Piece::Height>(*(itPoolBegin + 2)),
				static_cast<Piece::Shape>(*(itPoolBegin + 3))
			));
		} while (std::next_permutation(itPoolBegin, itPoolEnd));
	}
}
