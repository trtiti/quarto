#include "Piece.h"

Piece::Piece(Body body, Color color, Height height, Shape shape):
	m_body(body), m_color(color), m_height(height), m_shape(shape)
{
	sizeof(Piece);
	static_assert(sizeof(*this) == 1, "Size of piece is not 1. ");
}

Piece::Piece(const Piece& other)
{
	/*m_body = other.m_body;
	m_color = other.m_color;
	m_height = other.m_height;
	m_shape = other.m_shape;*/
	*this = other;
}

Piece::Piece(Piece&& other)
{
	*this = std::move(other);
}

Piece& Piece::operator=(const Piece& other)
{
	//new(this)Piece(other); emplacement new

	m_body = other.m_body;
	m_color = other.m_color;
	m_height = other.m_height;
	m_shape = other.m_shape;
	std::cout << "Object was coppied!\n";
	return *this;
}

Piece& Piece::operator=(Piece&& other)
{
	m_body = other.m_body;
	m_color = other.m_color;
	m_height = other.m_height;
	m_shape = other.m_shape;
	//reset
	memset(&other, 0, sizeof(other));
	return *this;
}

Piece::~Piece()
{
}

Piece::Body Piece::GetBody() const
{
	return m_body;
}

Piece::Color Piece::GetColor() const
{
	return m_color;
}

Piece::Height Piece::GetHeight() const
{
	return m_height;
}

Piece::Shape Piece::GetShape() const
{
	return m_shape;
}

std::ostream& operator<<(std::ostream& out, const Piece& piece)
{
	
	//out << static_cast<int16_t>(piece.m_body);
		//<< piece.m_color << piece.m_height << piece.m_shape;
	//return out;
	switch (piece.m_body)
	{
	case Piece::Body::Hollow:
		out << "Ho";
		break;
	case Piece::Body::Full:
		out << "Fu";
		break;
	default:
		throw "Invalid body type! ";
	}
	switch (piece.m_color)
	{
	case Piece::Color::Dark:
		out << "Da";
		break;
	case Piece::Color::Light:
		out << "Li";
		break;
	default:
		throw "Invalid color type! ";
	}
	switch (piece.m_height)
	{
	case Piece::Height::Short:
		out << "So";
		break;
	case Piece::Height::Tall:
		out << "Ta";
		break;
	default:
		throw "Invalid height type! ";
	}
	switch (piece.m_shape)
	{
	case Piece::Shape::Round:
		out << "Ro";
		break;
	case Piece::Shape::Square:
		out << "Sq";
		break;
	default:
		throw "Invalid height type! ";
	}
	return out;
}
