#pragma once
#include <iostream>
#include "UnusedPieces.h"
#include "Board.h"

class Player
{
public:
	Player(const std::string& name);
	Piece PickPiece(std::istream& in, UnusedPieces& unusedPiece);
	void PlacePiece(std::istream& in, Board& board, Piece&& piece);
	friend std::ostream& operator<<(std::ostream& out, const Player& player);
private:
	std::string m_name;

};

