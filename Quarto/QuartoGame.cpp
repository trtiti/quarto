#include "QuartoGame.h"
#include "Player.h"
#include "UnusedPieces.h"
#include "Board.h"

void QuartoGame::Run()
{
	std::string playerName;
	std::cin >> playerName;
	Player player1(playerName);
	std::cin >> playerName;
	Player player2(playerName);

	Board board;
	UnusedPieces unusedPieces;

	std::reference_wrapper<Player> pickerPlayer = player1;
	std::reference_wrapper<Player> placerPlayer = player2;

	while (true)
	{
		std::optional<Piece> piece;
		try
		{
			piece = pickerPlayer.get().PickPiece(std::cin, unusedPieces);
		}
		catch (const char* message)
		{
			std::cout << message << "\n";
		}

		try
		{
			placerPlayer.get().PlacePiece(std::cin, board, std::move(*piece));//piece.value()
		}
		catch (const std::out_of_range & exception)
		{
			std::cout << "*** " << exception.what() << "\n";
		}
		catch (const std::logic_error & exception)
		{
			std::cout << exception.what() << "\n";
		}

		//verify board

		std::swap(pickerPlayer, placerPlayer);
	}
}
