#include "pch.h"
#include "CppUnitTest.h"
#include "../Quarto/Piece.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuartoTests
{
	TEST_CLASS(PieceTests)
	{
	public:
		
		TEST_METHOD(Constructor)
		{
			Piece piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Tall, Piece::Shape::Round);
			Assert::IsTrue(piece.GetBody() == Piece::Body::Full);
			Assert::IsTrue(piece.GetColor() == Piece::Color::Dark);
			Assert::IsTrue(piece.GetHeight() == Piece::Height::Tall);
			Assert::IsTrue(piece.GetShape() == Piece::Shape::Round);
		}

		TEST_METHOD(Print)
		{
			Piece piece(Piece::Body::Full, Piece::Color::Dark, Piece::Height::Tall, Piece::Shape::Round);
			std::stringstream stream;
			stream << piece;
			Assert::AreEqual(std::string("FuDaTaRo"), stream.str(), L"If you see this message, piece name is not the same.");
		}
	};
}
