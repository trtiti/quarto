#include "pch.h"
#include "CppUnitTest.h"
#include "../Quarto/Piece.h"
#include "../Quarto/Board.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace QuartoTests
{
	TEST_CLASS(BoardTests)
	{
	public:
		
		TEST_METHOD(Constructor)
		{
			Board ourBoard;
			Assert::IsFalse(ourBoard[{1, 2}].has_value());
		}
	};
}
